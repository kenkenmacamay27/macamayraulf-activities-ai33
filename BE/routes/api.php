<?php

use App\Http\Controllers\BookController;
use App\Http\Controllers\BorrowedBookController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PatronController;
use App\Http\Controllers\ReturnedBookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resources([
    'books' => BookController::class,
    'patrons' => PatronController::class,
]);

Route::resource('categories', CategoryController::class)->only(['index']);

Route::resource('borrowedbook', BorrowedBookController::class)->only([
    'index', 'store'
]);

Route::resource('retunedbook', ReturnedBookController::class)->only([
    'index', 'store'
]);