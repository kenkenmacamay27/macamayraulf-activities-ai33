<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookRequest;
use App\Models\Book;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return response()->json(Book::with(['category:id,category'])->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        Book::create($request->all());
        return response()->json(['message' => 'Book successfully added.', 'book' => $book->with(['category:id,category'])->where('id', $book->id)->firstOrFail()], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        try {

            return response()->json(Book::with(['category:id,category'])->where('id', $id)->firstOrFail());

        } catch (ModelNotFoundException $exception) {

            return response()->json(['message' => 'Book not found.'], 404);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, $id)
    {
        
        try {

            $book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
            $book->update($request->validated());

            return response()->json(['message' => 'Book updated successfully!', 'book' => $book->with(['category:id,category'])->where('id', $book->id)->firstOrFail()]);

        } catch (ModelNotFoundException $exception) {

            return response()->json(['message' => 'Book not found'], 404);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        try {

            $book = Book::where('id', $id)->firstOrFail();
            $book->delete();

            return response()->json(['message' => 'Book deleted successfully!']);

        } catch (ModelNotFoundException $exception) {

            return response()->json(['message' => 'Book not found.'], 404);

        }
    }
}
