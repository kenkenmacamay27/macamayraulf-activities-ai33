<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\BorrowedBook;
use Illuminate\Http\Request;

class BorrowedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return response()->json(BorrowedBook::with(['patron', 'book', 'book.category'])->get());

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $createborrowed = BorrowedBook::create($request->only(['book_id', 'copies', 'patron_id']));

         $borrowedbook = BorrowedBook::with(['book'])->find($createborrowed->id);
         $borrowedbook->book->update(['copies' => $borrowedbook->book->copies - $request->copies]);
 
         return response()->json(['message' => 'Book borrowed successfully!', 'borrowed' => $borrowedbook], 201);
    }



}
