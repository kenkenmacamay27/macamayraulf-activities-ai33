<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\BorrowedBook;
use App\Models\ReturnedBook;
use Illuminate\Http\Request;

class ReturnedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return response()->json(ReturnedBook::with(['book', 'patron', 'book.category'])->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $borrowedbook = BorrowedBook::where([

            ['book_id', $request->book_id],

            ['patron_id', $request->patron_id],

        ])->firstOrFail();

        if(!empty($borrowedbook))

        {

            if($borrowedbook->copies == $request->copies){

                $borrowedbook->delete();

            }
            else
            {
                $borrowedbook->update(['copies' => $borrowedbook->copies - $request->copies]);

            }   

            $create_returned = ReturnedBook::create($request->only(['book_id', 'copies', 'patron_id']));

            $returnedbook = ReturnedBook::with(['book'])->find($create_returned->id);

            $returnedbook->book->update(['copies' => $returnedbook->book->copies + $request->copies]);

            return response()->json(['message' => 'Book returned successfully!', 'returned' => $returnedbook]);

        }  
    }
}
