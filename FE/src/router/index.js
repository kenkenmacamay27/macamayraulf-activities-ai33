import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Dashboard",
    component: () =>
    import(/* webpackChunkName: "dashboard" */ "@/components/pages/Dashboard.vue"),
  },
  {
    path: "/patron",
    name: "Patron",
    component: () =>
      import(/* webpackChunkName: "patron" */ "@/components/pages/Patron.vue"),
  },
  {
    path: "/book",
    name: "Book",
    component: () =>
      import(/* webpackChunkName: "book" */ "@/components/pages/Book.vue"),
  },
  {
    path: "/settings",
    name: "Settings",
    component: () =>
      import(/* webpackChunkName: "settings" */ "@/components/pages/Settings.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
