import Vue from "vue";
import store from "./store";
import { BootstrapVue } from "bootstrap-vue";
import toastr from "@/assets/js/toastr";

import App from "./App.vue";
import router from "./router";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "@/assets/css/style.css";
import "toastr/build/toastr.css";

Vue.use(BootstrapVue);
Vue.use(toastr);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
