import axios from "@/config/axios.js";
import toastr from "toastr";

const state = {
  books: [],
};

const getters = {
  getBooks: (state) => state.books,
};

const mutations = {
  setAddBook: (state, book) => state.books.unshift({ ...book }),
  setBooks: (state, books) => (state.books = books),
  setDeleteBook: (state, id) =>
    (state.books = state.books.filter((book) => book.id !== id)),
  setUpdateBook: (state, updatedBook) => {
    const index = state.books.findIndex((book) => book.id === updatedBook.id);
    if (index !== -1) {
      state.books.splice(index, 1, { ...updatedBook });
    }
  },
};

const actions = {
  async addBook({ commit }, book) {
    await axios
      .post("/books", book)
      .then((res) => {
        commit("setAddBook", res.data.book);
        toastr.success(res.data.message, "Success");
      })
      .catch((err) => {
        console.error(err);
      });
  },
  async allBooks({ commit }) {
    await axios
      .get("/books")
      .then((res) => {
        commit("setBooks", res.data);
      })
      .catch((err) => {
        console.error(err);
      });
  },
  async deleteBook({ commit }, id) {
    await axios
      .delete(`/books/${id}`)
      .then((res) => {
        commit("setDeleteBook", id);
        toastr.success(res.data.message, "Success");
      })
      .catch((err) => {
        console.error(err);
      });
  },
  async updateBook({ commit }, book) {
    var id = book.id;
    var updatedBook = {
      name: book.name,
      author: book.author,
      copies: book.copies,
      category_id: book.category_id,
    };
    await axios
      .put(`/books/${id}`, updatedBook)
      .then((res) => {
        commit("setUpdateBook", res.data.book);
        toastr.success(res.data.message, "Success");
      })
      .catch((err) => {
        console.error(err);
      });
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
