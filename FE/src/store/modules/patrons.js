import axios from "@/config/axios.js";
import toastr from "toastr";

const state = {
  patrons: [],
};

const getters = {
  getPatrons: (state) => state.patrons,
};

const mutations = {
  setAddPatron: (state, patron) => state.patrons.unshift({ ...patron }),
  setBooks: (state, patrons) => (state.patrons = patrons),
  setDeletePatron: (state, id) =>
    (state.patrons = state.patrons.filter((patron) => patron.id !== id)),
  setUpdatePatron: (state, updatedPatron) => {
    const index = state.patrons.findIndex(
      (patron) => patron.id === updatedPatron.id
    );
    if (index !== -1) {
      state.patrons.splice(index, 1, { ...updatedPatron });
    }
  },
};

const actions = {
  async addPatron({ commit }, patron) {
    await axios
      .post("/patrons", patron)
      .then((res) => {
        commit("setAddPatron", res.data.patron);
        toastr.success(res.data.message, "Success");
      })
      .catch((err) => {
        console.error(err);
      });
  },
  async allPatrons({ commit }) {
    await axios
      .get("/patrons")
      .then((res) => {
        commit("setPatrons", res.data);
      })
      .catch((err) => {
        console.error(err);
      });
  },
  async deletePatrons({ commit }, id) {
    await axios
      .delete(`/patrons/${id}`)
      .then((res) => {
        commit("setDeletePatron", id);
        toastr.success(res.data.message, "Success");
      })
      .catch((err) => {
        console.error(err);
      });
  },
  async updatePatron({ commit }, patron) {
    var id = patron.id;
    var updatedPatron = {
      last_name: patron.last_name,
      first_name: patron.first_name,
      middle_name: patron.middle_name,
      email: patron.email,
    };
    await axios
      .put(`/patrons/${id}`, updatedPatron)
      .then((res) => {
        commit("setUpdatePatron", res.data.patron);
        toastr.success(res.data.message, "Success");
      })
      .catch((err) => {
        console.error(err);
      });
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
