import axios from "@/config/axios.js";
import toastr from "toastr";

const state = {
  returned: [],
};

const getters = {};

const mutations = {
  setBorrowedBook: (state, returned) => state.returned.push({ ...returned }),
  setBorrowedBooks: (state, returnedbooks) => (state.returned = returnedbooks),
};

const actions = {
  async returnBook({ commit }, returned) {
    await axios
      .post("/retunedbook", returned)
      .then((res) => {
        toastr.success(res.data.message, "Success");
        commit("setRetunedBook", res.data.returned);
      })
      .catch((err) => {
        if (err.response.status == 422) {
          for (var i in err.response.data.errors) {
            toastr.error(err.response.data.errors[i][0], "Failed");
          }
        }
      });
  },
  
};

export default {
  state,
  getters,
  mutations,
  actions,
};
