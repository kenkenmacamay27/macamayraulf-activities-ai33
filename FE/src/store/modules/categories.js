import axios from "@/config/axios.js";

const state = {
  categories: [],
};

const getters = {
  getCategories: (state) => state.categories,
};

const mutations = {
  setCategories: (state, patrons) => (state.patrons = patrons),
};

const actions = {
  async allPatrons({ commit }) {
    await axios
      .get("/categories")
      .then((res) => {
        commit("setCategories", res.data);
      })
      .catch((err) => {
        console.error(err);
      });
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
