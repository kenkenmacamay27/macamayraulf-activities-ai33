import axios from "@/config/axios.js";
import toastr from "toastr";

const state = {
    borrowed: [],
  };
  
  const getters = {
    getBorrowedBooks: (state) => state.borrowed,
  };
  
  const mutations = {
    setBorrowedBook: (state, borrowed) => state.borrowed.push({ ...borrowed }),
    setBorrowedBooks: (state, borrowedBooks) => (state.borrowed = borrowedBooks),
  };
  
  const actions = {
    async borrowBook({ commit }, borrow) {
      await axios
        .post("/borrowedbook", borrow)
        .then((res) => {
          toastr.success(res.data.message, "Success");
          commit("setBorrowedBook", res.data.borrowed);
        })
        .catch((err) => {
          if (err.response.status == 422) {
            for (var i in err.response.data.errors) {
              toastr.error(err.response.data.errors[i][0], "Failed");
            }
          }
        });
    },
    async fetchBorrowedBooks({ commit }) {
      await axios
        .get("/borrowedbook")
        .then((res) => {
          commit("setBorrowedBooks", res.data);
        })
        .catch((err) => {
          console.error(err);
        });
    },
  };

export default {
  state,
  getters,
  mutations,
  actions,
};
