import Vue from "vue";
import Vuex from "vuex";
import books from "./modules/books";
import borrowedbooks from "./modules/borrowedbooks";
import categories from "./modules/categories";
import patrons from "./modules/patrons";
import returnedbooks from "./modules/returnedbooks";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    books,
    borrowedbooks,
    categories,
    patrons,
    returnedbooks,
  },
});
