<?php

namespace App\Http\Controllers;

use App\Models\Recipe;
use App\Models\Rating;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RecipeController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('jwt');
    }

    public function index(){
        return response()->json(User::with(['recipes', 'recipes.category'])->where('id', Auth::id())->get());
         
    }

    public function timeline() {
        return response()->json(Recipe::with(['user', 'ratings'])->get());
    }

    public function store(Request $request){
        
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'featured_image' => 'required',
            'procedure' => 'required',
            'category_id' => 'required'
        ]);

        Recipe::create([
            'title' => $request->title,
            'featured_image' => $request->featured_image,
            'description' => $request->description,
            'procedure' => $request->procedure,
            'user_id' => Auth::id(),
            'category_id' => $request->category_id
        ]);
           
        return response()->json(['msg' => 'Success'], 200);
    }

    public function update(Request $request, $id){
        
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'procedure' => 'required',
            'category_id' => 'required',
            'featured_image' => 'required'
        ]);

        $data = [
            'title' => $request->title,
            'description' => $request->description,
            'procedure' => $request->procedure,
            'category_id' => $request->category_id,
            'featured_image' => $request->featured_image,
        ];

        if($request->featuredImage){
            $data['featured_image'] = $request->featuredImage;
        };

        Recipe::where('id', $id)->update($data);
    }

    public function destroy($id){
        $info = Recipe::where('id', $id)->first();
        $this->deleteFileFromServer($info['featuredImage']);
        Recipe::destroy($id);
    }

    public function rate(Request $request) {

        $this->validate($request, [
            'rating' => 'required',
        ]);

        Rating::create([
            'user_id' => Auth::id(),
            'rating' => $request->rating,
            'recipe_id' => $request->recipe_id
        ]);

    }

    public function deleteFileFromServer($filename){
        $filePath = public_path().'/uploads/'.$filename;
        if(file_exists($filePath)){
            @unlink($filePath);
        }
        return;
    }

    public function uploadFeaturedImage(Request $request){
        $picName = time().'.'.$request->file->extension();
        $request->file->move(public_path('uploads'), $picName);
        return $picName;
    }
}
