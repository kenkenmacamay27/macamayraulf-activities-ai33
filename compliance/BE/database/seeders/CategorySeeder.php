<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categories = [
            'Main Dish',
            'Salad',
            'Soup/Stew',
            'Cookies',
            'Cakes',
            'Pies'
        ];

        foreach($categories as $catergory) {
            Category::create(['name' => $catergory]);
        }
    }
}
