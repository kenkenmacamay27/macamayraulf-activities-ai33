import axios from '@/axios';
const AUTH = "auth";

export default {
    state: {
        token: localStorage.getItem('access_token') || '',
        user: []
    },
    getters: {
        GET_USER(state) {
            return state.user;
        },
        GET_TOKEN(state) {
            return state.token;
        },
    },
    mutations: {
        SET_USER(state, user) {
            state.user = user;
        },
        SET_TOKEN(state, token) {
            localStorage.setItem("access_token", token);
            state.token = token;
            const bearer_token = localStorage.getItem("access_token") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${bearer_token}`;
        },
        UNSET_USER(state) {
            localStorage.removeItem("access_token");
            state.token = "";
            axios.defaults.headers.common["Authorization"] = "";
        },
    },
    actions: {
        async SIGNUP({ commit }, data) {
            const res = await axios.post(`${AUTH}/store`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async LOGIN({ commit }, user) {
            const res = await axios.post(`${AUTH}/login`, user)
                .then((response) => {
                    commit("SET_USER", response.data.user);
                    commit("SET_TOKEN", response.data.access_token);

                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async CHECKUSER({ commit }) {
            const res = await axios.get(
                `${AUTH}/me?token=` + localStorage.getItem("access_token")
            )
                .then((response) => {
                    commit("SET_USER", response.data);
                    commit("SET_TOKEN", localStorage.getItem("access_token"));
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async LOGOUT({ commit }) {  
            const res = await axios.post(`${AUTH}/logout?`)
                .then((response) => {
                    commit("UNSET_USER");
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
    }
}