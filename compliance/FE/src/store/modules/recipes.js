import axios from '@/axios';
const RECIPE = "recipes";
export default {
    state: {
        my_recipes: [],
        all_recipes: [],
        categories: []
    },
    getters: {
        GET_ALLRECIPES(state) {
            return state.all_recipes;
        },
        GET_MYRECIPES(state) {
            return state.my_recipes;
        },
        GET_CATEGORIES(state) {
            return state.categories
        }
    },
    mutations: {
        SET_MYRECIPES(state, my_recipes) {
            state.my_recipes = my_recipes;
        },
        SET_ALLRECIPES(state, all_recipes) {
            state.all_recipes = all_recipes;
        },
        SET_CATEGORIES(state, categories) {
            state.categories = categories;
        },
    },
    actions: {
        async FETCH_CATEGORIES({ commit }) {
            await axios.get("categories")
                .then((response) => {
                    commit("SET_CATEGORIES", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        async FETCH_ALLRECIPES({ commit }) {
            await axios.get(`/timeline`)
                .then((response) => {
                    commit("SET_ALLRECIPES", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        async FETCH_MYRECIPES({ commit }) {
            await axios.get(`${RECIPE}`)
                .then((response) => {
                    commit("SET_MYRECIPES", response.data[0].recipes);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        async ADD_RECIPE({ commit }, data) {
            const res = await axios.post(`${RECIPE}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async UPDATE_RECIPE({ commit }, { data, id }) {
            const res = await axios.put(`${RECIPE}/${id}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async DELETE_RECIPE({ commit }, id) {
            const res = await axios.delete(`${RECIPE}/${id}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async RATE_RECIPE({ commit }, data) {
            const res = await axios.post("/rate", data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
    }
}