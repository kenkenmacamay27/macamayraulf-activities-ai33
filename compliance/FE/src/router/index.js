import Vue from 'vue'
import VueRouter from 'vue-router'
import Timeline from '../views/Timeline.vue'
import MyRecipes from '../views/MyRecipes.vue'
import NewRecipe from '../views/NewRecipe.vue'
import UpdateRecipe from '../views/UpdateRecipe.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Timeline',
    component: Timeline
  },
  {
    path: '/recipes',
    name: 'MyRecipes',
    component: MyRecipes
  },
  {
    path: '/newrecipe',
    name: 'NewRecipe',
    component: NewRecipe
  },
  {
    path: '/updaterecipe',
    name: 'UpdateRecipe',
    component: UpdateRecipe,
    props: true
  },
  {
    path: '/welcome',
    name: 'LandingPage',
    component: () => import(/* webpackChunkName: "landingpage" */ '../views/LandingPage.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  },
  {
    path: '/signup',
    name: 'Signup',
    component: () => import(/* webpackChunkName: "signup" */ '../views/Signup.vue')
  },
  {
    path: '*',
    name: 'NotFound',
    component: () => import(/* webpackChunkName: "notfound" */ '../views/NotFound.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
