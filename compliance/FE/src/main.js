import Vue from 'vue'
import Vue2Editor from 'vue2-editor';
import App from './App.vue'
import router from './router'
import store from './store'
import { BootstrapVue } from "bootstrap-vue";
import VueFileAgent from 'vue-file-agent';
import VueFileAgentStyles from 'vue-file-agent/dist/vue-file-agent.css';
import Toast from "vue-toastification";



Vue.config.productionTip = false

import "vue-toastification/dist/index.css";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import '@/assets/css/style.css';

const options = {
  // You can set your default options here
  transition: "Vue-Toastification__bounce",
  maxToasts: 5,
  pauseOnHover: false,
  hideProgressBar: true,
};


Vue.use(Toast, options);
Vue.use(BootstrapVue);
Vue.use(Vue2Editor);
Vue.use(VueFileAgent);


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
